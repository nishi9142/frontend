import { Box } from '@chakra-ui/layout';
import React, { ReactNode } from 'react';

export enum WrapperVariant {
  regular = '800px',
  small = '400px',
}
interface Props {
  variant?: WrapperVariant;
  children: ReactNode;
}

const Wrapper: React.FC<Props> = ({
  children,
  variant = WrapperVariant.regular,
}: Props) => {
  return (
    <Box mx='auto' my='auto' maxW={variant} w='100%'>
      {children}
    </Box>
  );
};

export default Wrapper;
