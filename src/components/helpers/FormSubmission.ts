import { FormikHelpers } from 'formik';
import { getFormErrors } from '../../helpers/getFormErrors';

type AsyncFn<T> = () => Promise<T>;

interface Response {
  errors: any[];
  data: any;
}

export const handleSubmit = async <T, D>(
  values: T,
  actions: FormikHelpers<T>,
  apiCall: AsyncFn<D extends Response ? D : Response>,
  startCb?: AsyncFn<void> | null,
  endCb?: AsyncFn<void>
): Promise<void> => {
  const { setSubmitting, setErrors } = actions;
  setSubmitting(true);
  try {
    const response = await apiCall();
    const { errors, data } = response;
    if (errors) {
      setErrors(getFormErrors(errors));
    } else {
      if (endCb) await endCb();
      console.log({ data });
    }
  } catch (err) {
    console.log({ err });
  } finally {
    setSubmitting(false);
  }
};
