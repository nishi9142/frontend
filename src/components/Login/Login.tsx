import React from 'react';
import { Form, Formik } from 'formik';
import InputField from '../Form-Components/Input';
import * as Yup from 'yup';
import { Button } from '@chakra-ui/button';
import Wrapper, { WrapperVariant } from '../helpers/Wrapper';
import { useLoginMutation } from '../../generated/graphql';
import { handleSubmit } from '../helpers/FormSubmission';
import { useRouter } from 'next/router';

interface Props {}

const signUpValidationSchema = Yup.object().shape({
  username: Yup.string().min(4, '').required('Required'),
  password: Yup.string().required('Required'),
});

const Login = (props: Props) => {
  const [_, login] = useLoginMutation();
  const router = useRouter();
  const endCb = async () => {
    router.push('/');
  };
  return (
    <Wrapper variant={WrapperVariant.regular}>
      <Formik
        initialValues={{ username: '', password: '' }}
        onSubmit={async (values, actions) => {
          const apiCall = async () => {
            const response = await login({ options: values });
            return response.data?.login || {};
          };

          await handleSubmit<typeof values, any>(
            values,
            actions,
            apiCall,
            null,
            endCb
          );
        }}
        validationSchema={signUpValidationSchema}
      >
        {({ isSubmitting, errors }) => {
          const areErrors = !!Object.keys(errors).length;

          return (
            <Form>
              <InputField name='username' label='Username'></InputField>
              <InputField
                name='password'
                type='password'
                label='Password'
              ></InputField>
              <Button
                mt={4}
                colorScheme='teal'
                type='submit'
                isLoading={isSubmitting}
                disabled={areErrors}
              >
                Login
              </Button>
            </Form>
          );
        }}
      </Formik>
    </Wrapper>
  );
};

export default Login;
