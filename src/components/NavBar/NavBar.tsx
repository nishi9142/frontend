import { Box, Flex, Link, Text } from '@chakra-ui/layout';
import React, { useContext } from 'react';
import NextLink from 'next/link';
import { AuthContext } from '../../Context/AuthProvider';
import { Button } from '@chakra-ui/button';

interface Props {}

export const NavBar: React.FC<Props> = (props) => {
  const { state } = useContext(AuthContext) || {};
  console.log({ state });
  return (
    <Flex background={'turquoise'} p={5}>
      <Box ml={'auto'}>
        {!state?.me?.id ? (
          <>
            <NextLink href='/signin'>
              <Link mr={2}> Sign In </Link>
            </NextLink>
            <NextLink href='/signup'>
              <Link>Sign up</Link>
            </NextLink>
          </>
        ) : (
          <Flex>
            <Text mr={2}> {state?.me?.username} </Text>
            <Button type='button' variant='link'>
              {' '}
              Log out
            </Button>
          </Flex>
        )}
      </Box>
    </Flex>
  );
};
