import React, { InputHTMLAttributes } from 'react';
import {
  Box,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Input,
} from '@chakra-ui/react';
import { useField } from 'formik';
interface Props extends InputHTMLAttributes<{}> {
  name: string;
  label: string;
  placeholder?: string;
}

const InputField = ({ size, ...props }: Props) => {
  const [field, meta] = useField(props);

  const { placeholder, label } = props;
  const { error } = meta;

  return (
    <Box mt={4}>
      <FormControl isInvalid={!!error}>
        <FormLabel htmlFor={field.name}>{label}</FormLabel>
        <Input
          {...props}
          {...field}
          id={field.name}
          placeholder={placeholder ? placeholder : label}
        />
        {error && <FormErrorMessage> {error}</FormErrorMessage>}
      </FormControl>
    </Box>
  );
};

export default InputField;
