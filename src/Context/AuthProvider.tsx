import React, { createContext, useEffect, useReducer } from 'react';
import { MeQuery, useMeQuery } from '../generated/graphql';
import { UseQueryState } from 'urql';

interface Props {}

export interface ActionContext<T> {
  type: string;
  payload: T;
}

enum AuthEnum {
  CHANGE = 'change',
}

interface UserState {
  id: string;
  username: string;
}

interface AuthState {
  state: MeQuery | null;
}

const initalValue = { state: null };
const AuthContext = createContext<AuthState>(initalValue);
AuthContext.displayName = 'Auth Context';

const authReducer = (
  state: AuthState,
  action: ActionContext<MeQuery>
): AuthState => {
  switch (action.type) {
    case AuthEnum.CHANGE:
      return {
        ...state,
        state: action.payload,
      };
    default:
      return state;
  }
};
const AuthProvider: React.FC<Props> = (props) => {
  const [authState, dispatch] = useReducer(authReducer, initalValue);
  const [response] = useMeQuery();

  const authVerify = async (response: UseQueryState<MeQuery, object>) => {
    if (response.data) {
      dispatch({ type: AuthEnum.CHANGE, payload: response.data });
    }
  };

  useEffect(() => {
    console.log({ response });
    if (response) {
      authVerify(response);
    }

    return () => {};
  }, [response]);

  return (
    <AuthContext.Provider value={authState}>
      {props.children}
    </AuthContext.Provider>
  );
};

export { AuthContext, AuthProvider };
