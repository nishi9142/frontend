import { useContext } from 'react';
import { Container } from '../components/Container';
import { Login } from '../components/Login';
import { AuthContext } from '../Context/AuthProvider';
import { useRouter } from 'next/router';

const SignIn = () => {
  //   const { state } = useContext(AuthContext);
  //   const router = useRouter();

  return <Container height='100vh'>{<Login></Login>}</Container>;
};

export default SignIn;
