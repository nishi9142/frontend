import { ChakraProvider } from '@chakra-ui/react';
import theme from '../theme';
import { AppProps } from 'next/app';
import { AuthProvider } from '../Context/AuthProvider';
import { NavBar } from '../components/NavBar';
import { createClient, dedupExchange, fetchExchange, Provider } from 'urql';
import {
  cacheExchange,
  Cache,
  QueryInput,
  query,
} from '@urql/exchange-graphcache';
import {
  LoginMutation,
  MeDocument,
  MeQuery,
  RegisterUserMutation,
} from '../generated/graphql';
import { baseUrl } from '../constants';

function myUpdateQuery<Result, Query>(
  result: any,
  cache: Cache,
  qi: QueryInput,
  fn: (r: Result, data: Query) => Query
): void {
  cache.updateQuery(qi, (data) => fn(result, data as any) as any);
}

const client = createClient({
  url: `http://localhost:4000/graphql`,
  fetchOptions: {
    credentials: 'include',
  },
  exchanges: [
    dedupExchange,
    cacheExchange({
      updates: {
        Mutation: {
          login: (_result, args, cache, info) => {
            myUpdateQuery<LoginMutation, MeQuery>(
              _result,
              cache,
              { query: MeDocument },
              (result, data) => {
                if (result.login.errors) {
                  return data;
                } else {
                  return { me: result.login?.user } as MeQuery;
                }
              }
            );
          },
          registerUser: (_result, args, cache, info) => {
            myUpdateQuery<RegisterUserMutation, MeQuery>(
              _result,
              cache,
              { query: MeDocument },
              (result, data) => {
                if (result.registerUser.errors) {
                  return data;
                } else {
                  return { me: result.registerUser?.user } as MeQuery;
                }
              }
            );
          },
        },
      },
    }),
    fetchExchange,
  ],
});

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <Provider value={client}>
      <AuthProvider>
        <ChakraProvider resetCSS theme={theme}>
          <NavBar></NavBar>
          <Component {...pageProps} />
        </ChakraProvider>
      </AuthProvider>
    </Provider>
  );
}

export default MyApp;
