import { Container } from '../components/Container';
import { Signup } from '../components/Login';
import { AuthContext } from '../Context/AuthProvider';
import { useRouter } from 'next/router';
import { useContext } from 'react';

const Register = () => {
  // const { state } = useContext(AuthContext);
  // const router = useRouter();

  return (
    <Container height='100vh'>
      <Signup></Signup>
    </Container>
  );
};

export default Register;
