export const getFormErrors = (errors: any[]) => {
  return errors.reduce((ac, error) => {
    if (error?.field) {
      return { ...ac, [error.field]: error.message };
    }
    return ac;
  }, {});
};
