## Getting started

- For now please run backend in `http://localhost:4000/` (will change with env as nextjs needs more config just adding env)
- Use Node Version `15.*`
- Change `local.env` to `.env`--- (optional for now)
- Run `npm run install` --- installs node modules
- Run `npm run gen` --- installs all graphql schema & reslovers
- Run `npm run dev` --- start project in dev mode
